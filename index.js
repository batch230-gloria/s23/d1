console.log("================= OBJECTS =====================");

// [SECTION] Objects
/*
	An object is a data type that is used to represent real world object
	It is a collection of related data and/or functionalities
	Information is stored in object represented in key:value pair
		// key -> property of the object
		// value -> actual data to be stored

	Two ways in creating object in javascript
	1. Object literal notation (let object = {})
	2. Object Constructor notation
		// Object Instantation (let object = new Object();)
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
}
/*
	name is a property or key with value "Nokia 3210"
	manufactureDate is a property or key with value 1999
*/
console.log(" Result from creating objects using initializers/literal notations: ");
console.log(cellphone);
console.log(typeof cellphone);

let cellphone2 = {
	name: "Iphone13",
	manufactureDate: 2021
}
console.log(cellphone2);
console.log("---------------- Object Literal Notation -------------------")
/*
	Create an object with a name cellphone3,
	with property name and value "Xiaomi 11th Pro"
	and another key 'manufactureDate' and value 2021
*/

let cellphone3 = {
	name: "Xiaomi 11th Pro",
	manufactureDate: 2021
}
console.log(cellphone3);


console.log("--------------------- Object Constructor ------------------------");
// Object Constructor Notation
/*
	Creating objects using a constructor function
		- Creates a reusable function to create several objects that have the same data structure (blueprint)

	Syntax:
		function objectName(keyAm keyB){
			this.keyA = keyA;
			this.keyB = keyB;
		}

		// "this" keyword refers to the properties within the object
		// It allows that assignment of new object's properties by associating them with values received from the constructor function's parameter.
*/

function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}

let laptop1 = new Laptop("Lenovo", 2008)
console.log(laptop1);

let laptop2 = new Laptop("Macbook Air", 2008)
console.log(laptop2);

let laptop3 = new Laptop("Portal R2E CCMC", 1980)
console.log(laptop3);


console.log("---------- Empty Objects ----------")
// Create an empty objects
let computer = {};
let myComputer = new Object();

console.log("=============== Accessing Objects Properties =================")
// [SECTION] Accessing Object Properties / Keys

console.log("------------------ Dot Notation ------------------")
// Using dot notation
// Syntax. objectName.propertyName();
console.log("Result from dot notation: " + laptop2.name);

console.log("------------------ Square Bracket Notation ------------------")
// Using square bracket notation
console.log("Result from square bracket notation: " + laptop2["name"]);


console.log("---------------- Accessing array of objects ----------------")
// Accessing array of object
// Accessing oject properties using the square bracket notation and array indexes can cause confusion

let arrayObj = [laptop1, laptop2];

console.log(arrayObj[0]["name"]);
console.log(arrayObj[0].name);




console.log("=============== Initializing / Adding / Deleting / Reassigning Object Properties =================")

// [SECTION] Initializing / Adding / Deleting / Reassigning Object Properties

	let car = {};
	console.log("Current value of car object: ");
	console.log(car);

	console.log("--------- Initializing/adding object properties using dot notation ---------")
	// Initializing/adding object properties
	car.name = "Honda Civic";
	console.log("Result from adding properties using dot notation");
	console.log(car);


	console.log("--------- Initializing/adding object using bracket notation ---------")
	// Initializing/adding object using square bracket notation [Not recommended]
	car['manufacture date'] = 2019;
	console.log("Result from adding properties using square bracket notation");
	console.log(car);

	console.log("--------- Deleting object properties ---------")
	// Deleting object properties
	delete car['manufacture date'];
	console.log("Resilt from deleting properties");
	console.log(car);

	console.log("--------- Reassigning object properties ---------")
	// Reassigning object property values
	//		- same with initializing object properties
	car.name = "Toyota Vios";
	console.log("Result from adding properties using dot notation");
	console.log(car);






console.log("=============== Object Methods ===============");
// [SECTION] Object Methods
	// A method is function which is a property of an object

	let person = {
			name: "John",
			talk: function(){
				console.log("Hello my name is "+ this.name);
			}
	}

	console.log(person);
	person.talk();

	console.log("----- creating new properties in existing mode");
	// creating new property/key in an existing mode
	person.walk = function(steps){
			console.log(this.name + "walked " + steps + " steps forward");
	}
	console.log(person);
	person.walk(50);


	let friend = {
		firstName: "Joe",
		lastName: "Smith",
		address:{
			city: "Austin",
			country: "Texas"
		},
		emails: ["joe@mail.com", "joesmith@mail.xyz"],
		introduce: function(){
			console.log("Hello my name is "+ this.firstName + " " + this.lastName + " I live in "+ this.address.city);
		}
	}

	friend.introduce();


console.log("=============== Real World Application of Objects ===============");
// [SECTION] Real World Application of Objects
/*
	Scenario:
	1. We would like to create a game that would have a several pokemon interact with each other.
	2. Every pokemon would have the same set of stats, properties and function.

	Stats:
	name
	level
	health = level * 2
	attack = level

*/

	// Create an object constructor to lessen the process in creating the pokemon
	function Pokemon(name, level){
		// Properties
		this.name = name;
		this.level = level;
		this.health = level*2;
		this.attack = level;

		// Method
		// "target" parameter represents another pokemon object
		this.tackle = function(target){
			console.log(this.name + "tackled" + target.name);
			target.health -= this.attack;
			console.log("targetPokemon's health is now reduced to "+ this.health);
		}
		this.faint = function(){
			console.log(this.name + "fainted");
		}
	}

	let pikachu = new Pokemon("Pikachu", 99);
	console.log(pikachu);

	let rattata = new Pokemon("Rattata", 5);
	console.log(rattata);

	pikachu.tackle(rattata);
	//Pokemon.tackle();
	//Pokemon.faint();